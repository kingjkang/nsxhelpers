﻿
#==============================================#
#Environment Variables                         #
#==============================================#
$usernamewithoutdomain = "administratorAccount"
$domain = "Hostname-56" 
$creds = Get-Credential -Message "dont forget domain password" -UserName "$($usernamewithoutdomain)@$($domain)"
$nsxCreds = Get-Credential -Message "this for nsx connect @fqdn" -UserName "$($usernamewithoutdomain)@$($domain)"
$vcenter = "vcsa-01a.domain"
$nsxmgr = "nsx manager ip"

#this function takes a single ipset value or range and converts it to specific ip
function convertIPSETS{
    param($ipEntry)

    $origip = "192.168"
    $convip = "192.168"

    $ipEntry = $ipEntry -replace $origip, $convip

    return $ipEntry
}

function convertNamingConvention{
    param($ipEntry)

    $origgtac = "name"
    $convgtac = "name"

    $ipEntry = $ipEntry -replace $origgtac, $convgtac


    return $ipEntry
}

#==============================================#
#Core  Functions                               #
#==============================================#

#imports required modules
function init{
    Write-Output "Importing modules"
    $date1 = $(Get-Date)
    dir  C:\Users\Documents\ps-master\=.= | Unblock-File
    Import-Module C:\Users\Documents\ps-master\=.=\PowerNSX.psd1
    Import-Module C:\Users\Documents\ps-master\=.=\PowerNSX.psm1

    Import-Module VMware.VimAutomation.Core
    Import-Module VMware.VimAutomation.Vds
    Import-Module VMware.VimAutomation.Storage
    Import-Module C:\Users\Documents\ps-master\NSXHelpers.psm1

    $date2 = $(Get-Date)
    $time = $date2 - $date1
    Write-Output "Importing modules took $($time.seconds) seconds."
}

function servers{
    if(!$global:DefaultVIServers){
        Write-Output "You are not connected to any vCenter Servers"
    } else {
        #$global:DefaultVIServers
        foreach($server in $global:DefaultVIServers){
            Write-Output "You are connected to $($server.Name) as $($server.User)"
        }
    }
}

#changes the prompt and colors of CLI
function prompt{
    $console = $Host.UI.RawUI
    $console.ForegroundColor = "magenta"
    $Host.PrivateData.ConsolePaneBackgroundColor = "darkblue"
    $host.PrivateData.ConsolePaneTextBackgroundColor = "darkblue"
    $currPath = pwd
    #"$($currPath) admin$ "
    $vis = " $($global:DefaultVIServers.name) "
    "$($currPath)$($vis)admin$ "
}

#connect to vcenter 1
function vcsaconnect{
    Write-Output "Connecting to $($vcenter)"
    $vc = Connect-VIServer $vcenter -Credential $creds
    servers
}

#disconnect from vcenter
function vbd{
    servers
    Write-Output "Disconnecting from servers"
    Disconnect-VIServer * -Confirm:$false
    servers
}

#connect to nsx 1
function nsxconnect{
    Write-Output "Connecting to NSX manager $($nsxmgr)"
    $ns = Connect-NsxServer -Server $nsxmgr -Credential $nsxCreds
}

#disconnect from nsxmgr
function nsxd{
    Write-Output "Disconnecting from nsxmanagers"
    Disconnect-NsxServer * -Confirm:$false
}

#==============================================#
#Convenience Functions                         #
#==============================================#

#attaches VIB to host and scans inventory
function attachVIB{
    [cmdletbinding()]
    param($vib)
    Write-Output "Attaching VIB $($vib)"
    $baseline = Get-Baseline -Name $vib
    $hosts = Get-VMHost
    Attach-Baseline -Entity $hosts -Baseline $baseline
    Write-Output "Completed attaching VIB $($vib)"
    Write-Output "Re-scanning inventory for compliance"
    Scan-Inventory -Entity $hosts
    Write-Output "Completed re-scanning inventory for compliance"
}

#patches the vibs that are specified
function patchVUM{
    [cmdletbinding()]
    param($vc, $cluster, $vib, $async)
    Write-Output "Starting patch of $($vib) on cluster $($cluster)"
    $baseline = Get-Baseline -Name $vib
    if($async -eq "true"){
        $hosts = Get-VMHost
        foreach($item in $hosts){
            Remediate-Inventory -Server $vc -Entity $item -Baseline -RunAsync -Confirm:$false
        }
    } else {
        Remediate-Inventory -Server $vc -Entity $cluster -Baseline -Confirm:$false
    }
    Write-Output "Completed patch of $($vib) on cluster $($cluster)"
}

#enable ssh on specified host
function sshEnable{
    param($hostname)
    Write-Output "Enabling SSH on $($hostname)"
    $enabled = Get-VMHostService -VMHost $hostname | Where-Object {$_.Label -eq "ssh"} | Start-VMHostService
    if($enabled.Running){
        Write-Output "Enabled SSH on $($hostname)"
    } else {
        Write-Output "Failed to enable SSH on $($hostname)"
    }
}

#helper boe prox magic get host cert script
function ihella{
    param(
        [parameter(Mandatory=$true)]
        [string] $Destination,
        [parameter()]
        [int]$Port=443
    )
    try{
        $client = New-Object Net.sockets.tcpclient
        $client.Connect($Destination, $Port)
        $stream = $client.GetStream()
        $callback = {Param($sender, $cert, $chain, $errors) return $true}
        $sslstream = [System.Net.Security.SslStream]::new($stream, $true, $callback)
        $sslstream.AuthenticateAsClient('')
        [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($sslstream.RemoteCertificate)
    } catch {
        Write-Warning $_
    } finally {
        $sslstream.Dispose()
        $client.Dispose()
    }
}

#helper magic cert script wrapped to select the subject
function hellaCerts{
    param($hostname)
    $cert = ihella $hostname
    $cert | select subject
}

#return cert for hosts connected to vc
function stoked{
    param($hostname)

    if(!$global:DefaultVIServers){
        vbc
    } 
    
    Write-Output "Starting to obtaining host Ccrtificates"
    if($hostname){
        hellaCerts $hostname
    } else {
        $hosts = Get-VMHost
        foreach($curr in $hosts){
            hellaCerts $curr
        }
    }
    Write-Output " "
    Write-Output "Completed obtaining host certificates"
}

#reset the chap password
function chappass{
    [cmdletbinding()]
    param($cluster, $chapName, $chapPass, $mutualChapName, $mutualChapPass)

    $esx = Get-Cluster $cluster | Get-VMHost

    $hba = $esx | Get-VMHostHba -Type iScsi | Where {$_.Model -eq "iSCSI Software Adapter"}

    $hba | set-vmhosthba -MutualChapEnabled $true -ChapType Required -ChapName $chapName -ChapPassword $chapPass -MutualChapName $mutualChapName -MutualChapPassword $mutualChapPass

    $esx | get-vmhoststorage -RescanAllHba
}

#============================================================================================#
#NSX HELPERS START                                                                           #
#============================================================================================#

#dependencies: powernsx, vmware.vimautomation.core, vmware.vimautomation.vds, vmware.vimautomation.storage
#this class needs to be imported when working with firewall functions
Class firewallHelper{
    #properties
    [String] $name
    [String] $source
    [String] $destination
    [String] $service
    [String] $section
    [String] $action
}

#this class needs to be imported when working with service functions
Class serviceHelper{
    #properties
    [String] $name
    [String] $protocol
    [String] $destinationPort
    [String] $sourcePort
    [String] $isuniversal

}

#this class needs to be imported when working with service functions
Class serviceGroupHelper{
    #properties
    [String] $name
    [String] $members
    [String] $isUniversal
}

#this class needs to be imported when working with ipsets functions
Class IPSetHelper{
    #properties
    [String] $name
    [String] $members
    [String] $isUniversal
}

#this class needs to be imported when working with securitygroups functions
Class DynamicSecurityGroupHelper{
    #properties
    [String] $setOperator
    [String] $criteriaOperator
    [String] $key
    [String] $criteria
    [String] $value
}

#this class needs to be imported when working with securitygroups functions
Class SecurityGroupHelper{
    #properties
    [String] $name
    [System.Object] $dynamicMembership
    [System.Object] $staticInclude
    [System.Object] $staticExclude
}

#this class needs to be imported when working with securitytags functions
Class securityTagHelper{
    #properties
    [String] $name
    [String] $description
}

#==============================================#
#NSX Objects ALL                               #
#==============================================#

function beamMeUp{
    Write-Host "please check your pwd" -fore Yellow
    $enclave = "na"
    $block = "na"
    
    Write-Host "Exporting NSX Objects" -fore Green

    #getting the user input to which enclave and block we are currently in
    while(($enclave -ne 'a') -and ($enclave -ne 'b') -and ($enclave -ne 'c')){
        $enclave = Read-Host "enter a,b,c for the ENCLAVE you are currently in"
    }
    #Write-Output "you are in $($enclave)"
    while(($enclave -eq "a" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3" -and $block -ne "4") -or ($enclave -eq "b" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3" -and $block -ne "4") -or ($enclave -eq "c" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3")){
        $block = Read-Host "enter 1,2,3,4 for the BLOCK you are currently in" 
    }
    
    #exporting services to csv in current directory 
    Write-Host "Starting Exporting NSX Services" -fore Green
    exportNSXServices -e $enclave -b $block
    Write-Host "Finished exporting NSX Services" -fore Green

    Write-Host "Starting exporting NSX Service Groups" -fore Green
    exportNSXServiceGroups -e $enclave -b $block
    Write-Host "Finished exporting NSX service groups" -fore Green

    Write-Host "Starting exporting NSX IPSets" -fore Green
    exportNSXIPSets -e $enclave -b $block
    Write-Host "Finished exporting NSX IPSets" -fore Green

    Write-Host "Starting exporting NSX Security Groups" -fore Green
    exportNSXSecurityGroups -e $enclave -b $block
    Write-Host "Finished exporting NSX Security Groups" -fore Green

    Write-Host "Starting exporting NSX Security tags" -fore Green
    exportNSXSecurityTags -e $enclave -b $block
    Write-Host "Finished exporting NSX Security tags" -fore Green

    Write-Host "Starting exporting NSX Firewall Rules" -fore Green
    exportNSXFirewallSection -e $enclave -b $block
    Write-Host "Finished exporting NSX Firewall Rules" -fore Green
}

function beamMeDown{
    Write-Host "please check your pwd" -fore Yellow
    $enclave = "na"
    $block = "na"
    
    Write-Host "Importing NSX Objects" -fore Green
    "[$(Get-Date)] importing nsx objects" | Out-File lumberjack.txt -Append

    #getting the user input to which enclave and block you are importing from
    while(($enclave -ne 'a') -and ($enclave -ne 'b') -and ($enclave -ne 'c')){
        $enclave = Read-Host "enter a,b,c for the ENCLAVE you are importing from"
    }

    #Write-Output "you are in $($enclave)"
    while(($enclave -eq "a" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3" -and $block -ne "4") -or ($enclave -eq "b" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3" -and $block -ne "4") -or ($enclave -eq "c" -and $block -ne "1" -and $block -ne "2" -and $block -ne "3")){
        $block = Read-Host "enter 1,2,3,4 for the BLOCK you are importing from" 
    }
    "[$(Get-Date)] you are importing objects from $($enclave) block$($block)" | Out-File lumberjack.txt -Append
    
    $input = "na"
    $basepath = (pwd).Path
    "[$(Get-Date)] your pwd is $($basepath)" | Out-File lumberjack.txt -Append
    while(($input -ne "e") -and ($input -ne "exit") -and ($input -ne "q") -and ($input -ne "quit")){
        $input = Read-Host "enter the nsx object you would like to import `nservice for service `nservicegroup for service group `nipset for ipset `nsecuritygroup for security group `nfirewallrules for firewall rules `nsecuritytags for security tags `nq to quit `nselection" 
        if($input -eq "service"){
            "[$(Get-Date)] starting importing services" | Out-File lumberjack.txt -Append
            Write-Host "Importing services" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-services.csv"
            importNSXServices($importPath)
            "[$(Get-Date)] completed importing services" | Out-File lumberjack.txt -Append
        } elseif($input -eq "servicegroup"){
            "[$(Get-Date)] starting importing service groups" | Out-File lumberjack.txt -Append
            Write-Host "Importing service groups" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-serviceGroups.csv"
            importNSXServiceGroups($importPath)
            "[$(Get-Date)] completed importing service groups" | Out-File lumberjack.txt -Append
        } elseif($input -eq "ipset"){
            "[$(Get-Date)] starting importing ipsets" | Out-File lumberjack.txt -Append
            Write-Host "Importing ipsets" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-ipsets.csv"
            importNSXIPSets($importPath)
            "[$(Get-Date)] completed importing ipsets" | Out-File lumberjack.txt -Append
        } elseif($input -eq "securitygroup"){
            "[$(Get-Date)] starting importing security groups" | Out-File lumberjack.txt -Append
            Write-Host "Importing security groups" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-securitygroups.csv"
            importNSXSecurityGroups($importPath)
            "[$(Get-Date)] completed importing security groups" | Out-File lumberjack.txt -Append
        } elseif($input -eq "firewallrules"){
            "[$(Get-Date)] starting importing firewall rules" | Out-File lumberjack.txt -Append
            Write-Host "Importing firewall rules" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-DFWR.csv"
            importNSXFirewallRules($importPath)
            "[$(Get-Date)] completed importing firewall rules" | Out-File lumberjack.txt -Append
        } elseif($input -eq "securitytags"){
            "[$(Get-Date)] starting importing security tags" | Out-File lumberjack.txt -Append
            Write-Host "Importing security tags" -fore Green
            $importPath = $basepath + "\$($enclave)b$($block)-securitytags.csv"
            importNSXSecurityTags($importPath)
            "[$(Get-Date)] completed importing security tags" | Out-File lumberjack.txt -Append
        }
    }
}

#==============================================#
#NSX Distrbuted Firewalls                      #
#==============================================#

#This will export an entire firewall section to a CSV file in the current directory you are in
#The file will be named [enclave-block-DFWR.csv]
#this function takes a string paramter with firewall section name
function exportNSXFirewallSection{
    [cmdletbinding()]
    param($e, $b, $ruleid)
    $enclaveColor = $e
    $blockNum = $b
    $rid = $ruleid
    if($rid){
        $mmhm = Get-NsxFirewallRule -RuleId $rid
    } else {
        $mmhm = Get-NsxFirewallRule
    }
    
    $n = $mmhm | select name, sources, destinations, services, action, sectionid
    $toprint = @()
    $index = 1
    foreach($i in $n){
        Write-Host "[$($index)/$($n.length)] Exporting $($i.name)" -fore Cyan
        $whatup = New-Object firewallHelper
        $whatup.name = $i.name

        $a = $i.sources | select -ExpandProperty source
        $ca = 0
        if($a.Length -eq 0){
            $whatup.source = "any"
        } else {
            foreach($x in $a){
                if($ca -eq 0){
                    if(($x.type -eq "ipv4address") -or ($x.type -eq "ipv6address")){
                        $whatup.source += "$(convertNamingConvention($x.value))&$($x.type)"
                        $ca++
                    } else {
                        $whatup.source += "$(convertNamingConvention($x.name))&$($x.type)"
                        $ca++
                    }
                } else {
                    if(($x.type -eq "ipv4address") -or ($x.type -eq "ipv6address")){
                        $whatup.source += ",$(convertNamingConvention($x.value))&$($x.type)"
                    } else {
                        $whatup.source += ",$(convertNamingConvention($x.name))&$($x.type)"
                    }
                }
            }
        }
        
        $b = $i.destinations | select -ExpandProperty destination
        $cb = 0

        if($b.length -eq 0){
            $whatup.destination = "any"
        } else {
            foreach($x in $b){
                if($cb -eq 0){
                    if(($x.type -eq "ipv4address") -or ($x.type -eq "ipv6address")){
                        $whatup.destination += "$(convertNamingConvention($x.value))&$($x.type)"
                        $cb++
                    } else {
                        $whatup.destination += "$(convertNamingConvention($x.name))&$($x.type)"
                        $cb++
                    }
                } else {
                    if(($x.type -eq "ipv4address") -or ($x.type -eq "ipv6address")){
                        $whatup.destination += ",$(convertNamingConvention($x.value))&$($x.type)"
                    } else {
                        $whatup.destination += ",$(convertNamingConvention($x.name))&$($x.type)"
                    }
                }
            }   
        }

        $c = $i.services | select -ExpandProperty service
        $cc = 0

        if($c.length -eq 0){
            $whatup.service = "any"
        } else {
            foreach($x in $c){
                if($cc -eq 0){
                    $whatup.service += "$($x.name)&$($x.type)"
                    $cc++
                } else {
                    $whatup.service += ",$($x.name)&$($x.type)"
                }
            }
        }

        $whatup.section = (Get-NsxFirewallSection -objectId $i.sectionid).name

        $whatup.action = $i.action
        
        $toprint += $whatup
        $index++
    }

    $fileName = "$($enclaveColor)b$($blockNum)-DFWR.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

function importNSXFirewallRules{
    param($importpath)
    $toyota = Import-Csv $importpath
    $index = 1

    foreach($camry in $toyota){
        Write-Host "[$($index)/$($toyota.length)] importing $($rav4.name)" -fore Cyan
        $iName = $camry.name
        $iSource = $camry.source
        $iDestination = $camry.destination
        $iService = $camry.service
        $iSection = $camry.section
        $iAction = $camry.action

        Write-Host "checking the firewall section $($iSection)" -fore Cyan
        if((Get-NsxFirewallSection -Name $iSection).name -notcontains $iSection){
            if((Get-NsxFirewallSection -Name $iSection) -match "::"){
                "[$(Get-Date)] manual input of $($iSection) needed, addition of firewall policy failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($iSection) needed, addition of firewall policy failed" -fore Red
            } else {
                $defSec = Get-NsxFirewallSection
                $defSecID = $defSec[$defSec.length-1]
                $nSection = New-NsxFirewallSection -Name $iSection -scopeId globalroot-0 -sectionType layer3sections -position before $defSecID.id 
                $nSection = Get-NsxFirewallSection -Name $iSection
            }
        } else {
            $nSection = Get-NsxFirewallSection -Name $iSection
        }

        Write-Host "checking the firewall rule members" -fore Cyan
        $controlset = (Get-NsxFirewallRule -Name $iName).name
        if($controlset -contains $iName){
            Write-Host "the firewall rule $($iname) already exists so checking and adding the src, dst, serv" -fore Yellow
            #check the sources 
            $rawr = $iSource.split(",")
            $nSource = @() 
            foreach($i in $rawr){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $i.Split("&")
                if($temp[1] -eq "IPSet"){
                    $setoctrl = ($controlset.sources.source | ?{$_.type -eq "ipset"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) source required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) sources" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Source -Member (Get-NsxIpSet -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "SecurityGroup"){
                    $setoctrl = ($controlset.sources.source | ?{$_.type -eq "securitygroup"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) source required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) sources" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Source -Member (Get-NsxSecurityGroup -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }
            #check the destinations
            $rawr = $iDestination.split(",")
            $nDestination = @() 
            foreach($i in $rawr){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $i.Split("&")
                if($temp[1] -eq "IPSet"){
                    $setoctrl = ($controlset.destinations.destination | ?{$_.type -eq "ipset"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) destination required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) destinations" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Destination -Member (Get-NsxIpSet -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "SecurityGroup"){
                    $setoctrl = ($controlset.destinations.destination | ?{$_.type -eq "securitygroup"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) destination required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) destinations" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Destination -Member (Get-NsxSecurityGroup -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }
            #check the services
            $woo = $iService.split(",")
            $nService = @()
            foreach($i in $woo){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $i.Split("&")
                if($temp[1] -eq "application"){
                    $setocontrol = ($controlset.services.service | ?{$_.type -eq "application"}).name
                    if($setocontrol -contains $temp[0]){
                        Write-Host "firewall rule $($iName) already contains $($temp[0])" -fore Yellow
                    } else {
                        "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                         Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                    }
                } elseif($temp[1] -eq "applicationgroup"){
                    $setocontrol = ($controlset.services.service | ?{$_.type -eq "applicationgroup"}).name
                    if($setocontrol -contains $temp[0]){
                        Write-Host "firewall rule $($iName) already contains $($temp[0])" -fore Yellow
                    } else {
                        "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                         Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                    }
                } else {
                    "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                    Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                }
            }
        } else {
            Write-Host "the firewall rule $($iname) does not already exist so creating the objects and creating new firewall rule" -fore Green
            $rawr = $iSource.split(",")
            $nSource = @() 
            foreach($i in $rawr){
                $temp = $i.Split("&")
                if($temp[1] -eq "IPSet"){
                    $nSource += Get-NsxIpSet -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "SecurityGroup"){
                    $nSource += Get-NsxSecurityGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }

            $omg = $iDestination.split(",")
            $nDestination = @()
            foreach($i in $omg){
                $temp = $i.Split("&")
                if($temp[1] -eq "IPSet"){
                    $nDestination += Get-NsxIpSet -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "SecurityGroup"){
                    $nDestination += Get-NsxSecurityGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }

            $woo = $iService.split(",")
            $nService = @()
            foreach($i in $woo){
                $temp = $i.Split("&")
                if($temp[1] -eq "Application"){
                    $nService += Get-NsxService -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "ApplicationGroup"){
                    $nService += Get-NsxServiceGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                }
            }

            try{

                if(($nSource -eq "any") -and ($nDestination -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any") -and ($nDestination -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Destination $nDestination -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Destination $nDestination -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nDestination -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nDestination -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Destination $nDestination -Action $iAction -EnableLogging -Position Bottom
                }else{
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Destination $nDestination -Service $nService -Action $iAction -EnableLogging -Position Bottom -Disabled
                }

            } catch {
                $lol = $false
            } finally {
                if($lol -eq $false){
                    Write-Output "Creation of rule $($camry.name) failed"
                } else {
                    Write-Host "Creation of rule $($camry.name) succeeded" -fore Cyan
                }
            }
        }

        $index++
    }
}

<#
function changes all logging status for firewall rules to true
eagleEye -vcip 000.000.000.103 -nsxMgrIP 000.000.000.104
eagleEye -vcip 000.000.000.105 -nsxMgrIP 000.000.000.106
eagleEye -vcip 000.000.000.107 -nsxMgrIP 000.000.000.108
eagleEye -vcip 000.000.000.109 -nsxMgrIP 000.000.000.110
eagleEye -vcip 000.000.000.111 -nsxMgrIP 000.000.000.112
Get-NsxFirewallRule | select name, id, logged
#>
function eagleEye{
    [cmdletbinding()]
    param($vcip, $nsxMgrIP)

    $vc = Connect-VIServer $vcip -Credential $(Get-Credential -Message "vcsa login credentials, please use fqdn" -UserName "ent\admin.kangj")
    $ns = Connect-NsxServer -Server $nsxMgrIP -Credential $(Get-Credential -Message "nsxmgr login credentials, please use fqdn" -UserName "email-695@acme.com")
    Write-Host "connected to nsx manager $($ns.Server) for vcenter $($ns.VIConnection)" -fore Cyan

    if($ns){
        $firewallSectionIDS = Get-NsxFirewallSection
        $ids = $firewallSectionIDS.id
        foreach($sid in $ids){
            Write-Host "changing section $($sid)" -fore Cyan
            $req = Invoke-NsxWebRequest -URI "/api/4.0/firewall/globalroot-0/config/layer3sections/$($sid)" -method get
            $content = [xml]$req.Content
            #$content.section.rule
            $rules = $content.section.rule | ?{$_.logged -eq "false"}
            foreach($rule in $rules){
                Write-Host "changing rule $($rule.name)" -fore Cyan
                ($content.section.rule | ?{$_.id -eq $rule.id}).logged = "true"
            }
            $addHeader = @{"If-Match"=$req.Headers.ETag}
            $response = Invoke-NsxWebRequest -URI "/api/4.0/firewall/globalroot-0/config/layer3sections/$($sid)" -method put -extraheader $addHeader -body $content.section.OuterXml
        }
    }

    Disconnect-NsxServer * -Confirm:$false
    Disconnect-VIServer * -Confirm:$false
}

function anyanyany{
    #$rulz = Get-NsxFirewallRule -RuleId 1636 | select name,id,sources,destinations,services
    $rulz = Get-NsxFirewallRule | select name,id,sources,destinations,services
    $tix = @()
    foreach($rules in $rulz){
        if(($rules.sources -eq $null) -or ($rules.destinations -eq $null) -or ($rules.services -eq $null)){
            $rules
            $tix += $rules
        }
    }
    $tix | Export-Csv rb4-jiratix.csv
}

#==============================================#
#NSX Grouping Objects - IPSets                 #
#==============================================#

#this function imports ipset from csv
function importNSXIPSets{
    param($importpath)
    $honda = Import-Csv $importpath
    $controlset = convertNamingConvention(((Get-NsxIpSet).name | ?{$_.isuniversal -eq "false"}))
    $index = 1
    foreach($rav4 in $honda){
        Write-Host "[$($index)/$($honda.length)] importing $($rav4.name)" -fore Cyan
        if($controlset -contains $rav4.name){
            $src = Get-NsxIpSet -Name $rav4.name
            $base = $src.value.split(',')
            $attack = $rav4.members.split(',')
            foreach($mem in $attack){
                $src = Get-NsxIpSet -Name $rav4.name
                $base = $src.value.split(',')
                if($base -contains $mem){
                    Write-Host "the ip $($mem) already exists, not adding" -fore Yellow
                } else {
                    $src = Get-NsxIpSet -Name $rav4.name
                    $added = Add-NsxIpSetMember -IPSet $src -IPAddress $mem
                    if($added){
                        Write-Host "adding ip $($mem) to ipset $($rav4.name)" -fore Green
                    } else {
                        "[$(Get-Date)] manual input of $($mem) to ipset $($rav4.name) needed, addition to ipset failed" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($mem) to ipset $($rav4.name) needed, addition to ipset failed" -fore Red
                    }
                }
            }
        } else {
            $added = New-NsxIpSet -Name $rav4.name -IPAddress $rav4.members
            if($added){
                Write-Host "adding ip $($rav4.members) to ipset $($rav4.name)" -fore Green
            } else {
                "[$(Get-Date)] manual input of $($rav4.members) to ipset $($rav4.name) needed, creation of ipset failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($rav4.members) to ipset $($rav4.name) needed, creation of ipset failed" -fore Red
            }
        }
        $index++
    }
}

#export ipset to csv
function exportNSXIPSets{
    [cmdletbinding()]
    param($e, $b)
    $pull = Get-NsxIpSet | ?{$_.isuniversal -eq "false"} #| ?{$_.name -match "justin"}
    $index = 1
    $toprint = @()

    foreach($ipset in $pull){
        Write-Host "[$($index)/$($pull.length)] Exporting $($ipset.name)" -fore Cyan
        $whatup = New-Object IPSetHelper
        $whatup.name = convertNamingConvention($ipset.name)
        $whatup.members = convertIPSETS($ipset.value)
        $whatup.isUniversal = $Hostname-243
        $toprint += $whatup
        $index++
    }

    $connected = $global:DefaultVIServer.Name.Split(".")
    $fileName = "$($e)b$($b)-ipsets.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#==============================================#
#NSX Grouping Objects - Security Groups        #
#==============================================#

function exportNSXSecurityGroups{
    [cmdletbinding()]
    param($e, $b)
    $pull = Get-NsxSecurityGroup | ?{$_.isuniversal -eq "false"} #| ?{$_.name -match "justin"}
    #$pull = Get-NsxSecurityGroup
    $index = 1
    $toprint = @()
    foreach($securityGroup in $pull){
        Write-Host "[$($index)/$($pull.length)] Exporting $($securityGroup.name)" -fore Cyan
        $sg = New-Object SecurityGroupHelper
        $sg.name = convertNamingConvention($securityGroup.name)
        #$sg.staticInclude = $securityGroup.member
        $staticIncludes = ""
        $count = 0
        foreach($static in $securityGroup.member){
            if($count -eq 0){
                $staticIncludes += "$(convertNamingConvention($static.name))&$($static.objectTypeName)"
                $count++
            } else {
                $staticIncludes += ",$(convertNamingConvention($static.name))&$($static.objectTypeName)"
            }
        }
        $sg.staticInclude = $staticIncludes
        #$staticIncludes
        #$sg.staticExclude = $securityGroup.excludeMember
        $staticExcludes = ""
        $count = 0
        foreach($static in $securityGroup.excludeMember){
            if($count -eq 0){
                $staticExcludes += "$(convertNamingConvention($static.name))&$($static.objectTypeName)"
                $count++
            } else {
                $staticExcludes += ",$(convertNamingConvention($static.name))&$($static.objectTypeName)"
            }
        }
        $sg.staticExclude = $staticExcludes
        #$staticExcludes
        #dybamicmembership
        $dynstring = ""
        $count = 0
        foreach($dynamicCriteria in $securityGroup.dynamicmemberdefinition.dynamicset){
            $dynCriteriaTemp = New-Object DynamicSecurityGroupHelper
            $strtoadd = ""
            #Write-Host "abou to do dynamic criteria $($dynamicCriteria.dynamiccriteria.length)" -fore Red

            if($dynamicCriteria.dynamiccriteria.length -gt 1){
                #this means multiple criteria within criteria
                #$dynamicCriteria
                $multicriteriastring = "$($dynamicCriteria.operator),"
                $count = 0
                foreach($dc in $dynamicCriteria.dynamiccriteria){
                #$dc
                    if($count -eq 0){
                        $multicriteriastring += "$($dc.operator),$($dc.key),$($dc.criteria),$(convertNamingConvention($dc.value))"
                        $count++
                    } else {
                        $multicriteriastring += "&$($dc.operator),$($dc.key),$($dc.criteria),$(convertNamingConvention($dc.value))"
                    }
                }
                $strtoadd = $multicriteriastring
            } else {
                $dynCriteriaTemp.setOperator = $dynamicCriteria.operator
                $dynCriteriaTemp.criteriaOperator = $dynamicCriteria.dynamicCriteria.operator
                $dynCriteriaTemp.key = $dynamicCriteria.dynamicCriteria.key
                $dynCriteriaTemp.criteria = $dynamicCriteria.dynamicCriteria.criteria
                $dynCriteriaTemp.value = $dynamicCriteria.dynamicCriteria.value
                $dynCriteriastring = "$($dynamicCriteria.operator),$($dynCriteriaTemp.criteriaOperator),$($dynCriteriaTemp.key),$($dynCriteriaTemp.criteria),$(convertNamingConvention($dynCriteriaTemp.value))"
                $strtoadd = $dynCriteriastring
            }
            if($count -eq 0){
                $dynstring += $strtoadd    
                $count++
            } else {
                $strtoadd = "%$($strtoadd)"
                $dynstring += $strtoadd
            }
        }
        $sg.dynamicMembership = $dynstring
        #$dynstring | Out-File "omfg.txt"
        $toprint += $sg
        $index++
    }

    $connected = $global:DefaultVIServer.Name.Split(".")
    $fileName = "$($e)b$($b)-securityGroups.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

function importNSXSecurityGroups{
    param($importpath)
    $honda = Import-Csv $importpath
    #$honda = Import-Csv C:\Users\IS_Admin\Documents\bunnyHopEnclave\b-securityGroups.csv
    $index = 1
    foreach($s2000 in $honda){
        Write-Host "[$($index)/$($honda.length)] importing $($s2000.name)" -fore Cyan
        #creating the security group
        if(!(Get-NsxSecurityGroup -name $s2000.name)){
            Write-Host "creating security group $($s2000.name)"  -fore Green
            $secgroup = New-NsxSecurityGroup -Name $s2000.name 
        } else {
            Write-Host "$($s2000.name) security group already exists" -fore Yellow
        }

        #adding the static include members
        $staticIncludes = $s2000.staticinclude.split(',')
        if($s2000.staticinclude.length -gt 0){
            foreach($si in $staticIncludes){
                $info = $si.split('&')
                if($info[1] -eq "VirtualMachine"){
                    if((Get-VM).name -contains $info[0]){
                        $toadd = Get-VM -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-244 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, virtual machine $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, virtual machine $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "IPSet"){
                    if((Get-NsxIpSet).name -contains $info[0]){
                        $toadd = Get-NsxIpSet -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-244 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, ipset $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, ipset $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityGroup"){
                    if((Get-NsxSecurityGroup).name -contains $info[0]){
                        $toadd = Get-NsxSecurityGroup -name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-244 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                         "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, security group $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, security group $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityTag"){
                    if((Get-NsxSecurityTag).name -contains $info[0]){
                        $toadd = Get-NsxSecurityTag -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-244 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                         "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, security tag $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, security tag $($info[0]) does not exist" -fore Red   
                    }
                } elseif($info[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($info[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, addition of cluster failed" -fore Red
                } elseif($info[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static include needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($s2000.name) static include needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) needed, $($info[1]) $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($s2000.name) needed, $($info[1]) $($info[0]) does not exist" -fore Red 
                }
            }
        }

        #adding the static exclude members
        $staticExcludes = $s2000.staticexclude.split(',')
        if($s2000.staticexclude.length -gt 0){
            foreach($si in $staticExcludes){
                $info = $si.split('&')
                if($info[1] -eq "VirtualMachine"){
                    if((Get-VM).name -contains $info[0]){
                        $toadd = Get-VM -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-245 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static exclude needed, virtual machine $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) needed, virtual machine $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "IPSet"){
                    if((Get-NsxIpSet).name -contains $info[0]){
                        $toadd = Get-NsxIpSet -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-245 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static exclude needed, ipset $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static exclude needed, ipset $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityGroup"){
                    if((Get-NsxSecurityGroup).name -contains $info[0]){
                        $toadd = Get-NsxSecurityGroup -name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-245 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static exclude needed, security group $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static exclude needed, security group $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityTag"){
                    if((Get-NsxSecurityTag).name -contains $info[0]){
                        $toadd = Get-NsxSecurityTag -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        if(!($Hostname-245 -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($s2000.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($s2000.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) static exclude needed, security tag $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($s2000.name) static exclude needed, security tag $($info[0]) does not exist" -fore Red 
                    }
                } else {
                    "[$(Get-Date)] manual input of $($info[0]) to $($s2000.name) needed, $($info[1]) $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($s2000.name) needed, $($info[1]) $($info[0]) does not exist" -fore Red 
                }
            }
        }
    
        #adding the dynamic membership
        $dynMembership = $s2000.dynamicmembership.split('%')
        $dynamicmembersetindex = 0
        if($s2000.dynamicmembership.length -gt 0){
            foreach($dynmem in $dynMembership){
                $dynmem = $dynmem -replace "vm.name","vmname"
                $dynmem = $dynmem -replace "vm.guest_os_full_name","osname"
                $dynmem = $dynmem -replace "vm.guest_host_name","computername"
                $dynmem = $dynmem -replace "vm.security_tag","securitytag"
                $dynmem = $dynmem -replace "contains","contains"
                $dynmem = $dynmem -replace "ends_with","ends_with"
                $dynmem = $dynmem -replace "starts_with","starts_with"
                $dynmem = $dynmem -replace "!=","notequals"
                $dynmem = $dynmem -replace "=","equals"
                $dynmem = $dynmem -replace "similar_to","regex"
                                                                                                                                                                                                                            if($dynmem -match "&"){
                #Write-Host "This dynamic membership criteria has MULTIPLE criteria" -fore White
                $multc = $dynmem.split('&')
                foreach($criterium in $multc){
                    $info = $criterium.split(',')
                    if($info[1] -eq "ENTITY"){
                        Write-Host "manual intervention needed, failed to add where $($info[0]) $($info[1]) $($info[2]) $($info[3]) to security group $($s2000.name)" -fore Red
                        "[$(Get-Date)] manual intervention needed, failed to add where $($info[0]) $($info[1]) $($info[2]) $($info[3]) to security group $($s2000.name)" | Out-File manualOverride.txt -Append
                    } elseif($info.length -eq 5){
                        $setOperator = $info[0]
                        $setOperator = $setOperator -replace "any","or"
                        $setOperator = $setOperator -replace "all","and"
                        $criteriaOperator = $info[1]
                        $criteriaOperator = $criteriaOperator -replace "or","any"
                        $criteriaOperator = $criteriaOperator -replace "and","all"
                        $key = $info[2]
                        $criteria = $info[3]
                        $value = $info[4]
                        $spec = $null
                        $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        #Write-Host "adding dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        $updateddynset = $sg | Add-NsxDynamicMemberSet -SetOperator $setOperator -CriteriaOperator $criteriaOperator -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                        $dynamicmembersetindex++
                        if($spec){
                            Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        } else {
                            Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($s2000.name)" -fore Red
                            "[$(Get-Date)] manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($s2000.name)" | Out-File manualOverride.txt -Append
                        }
                    } else {
                        $criteriaOperator = $info[0]
                        $criteriaOperator = $criteriaOperator -replace "or","any"
                        $criteriaOperator = $criteriaOperator -replace "and","all"
                        $key = $info[1]
                        $criteria = $info[2]
                        $value = $info[3]
                        $spec = $null
                        $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                        $sg = Get-NsxSecurityGroup -name $s2000.name
                        $dynmembersetgg = $sg | Get-NsxDynamicMemberSet -Index $dynamicmembersetindex
                        #Write-Host "adding dynamic criteria where $($criteriaoperator) $($key) $($criteria) $($value) to index $($dynamicmembersetindex)" -fore green
                        $updateddynset = $dynmembersetgg | Add-NsxDynamicCriteria -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                        if($updateddynset){
                            Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        } else {
                            Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($s2000.name)" -fore Red
                            "[$(Get-Date)] manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($s2000.name)" | Out-File manualOverride.txt -Append
                        }
                    }
                }
                        } elseif($dynmem -match "entity"){ 
                Write-Host "manual intervention needed, failed to add where criteria contains entity" -fore Red
                "[$(Get-Date)] manual addition where failed to add where criteria contains entity check security group $($s2000.name)" | Out-File manualOverride.txt -Append   
                                                                                                                } else {
                #Write-Host "This dynamic membership criteria has ONE criteria" -fore White
                $info = $dynmem.split(',')
                $setOperator = $info[0]
                $setOperator = $setOperator -replace "any","or"
                $setOperator = $setOperator -replace "all","and"
                $criteriaOperator = $info[1]
                $criteriaOperator = $criteriaOperator -replace "or","any"
                $criteriaOperator = $criteriaOperator -replace "and","all"
                $key = $info[2]
                $criteria = $info[3]
                $value = $info[4]
                $spec = $null
                $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                $sg = Get-NsxSecurityGroup -name $s2000.name
                #Write-Host "adding dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                $updateddynset = $sg | Add-NsxDynamicMemberSet -SetOperator $setOperator -CriteriaOperator $criteriaOperator -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                $updateddynset
                $dynamicmembersetindex++
                if($spec){
                    Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                } else {
                    Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value)" -fore Red
                    "[$(Get-Date)] manual addition where $($criteriaoperator) $($key) $($criteria) $($value) failed check security group $($s2000.name)" | Out-File manualOverride.txt -Append
                }
            }
            }
        }
        $index++
    }
}

#==============================================#
#NSX Grouping Objects - Services               #
#==============================================#

#this function exports services to csv
function exportNSXServices{
    #$pull = Get-NsxService -Name "justinRox"
    [cmdletbinding()]
    param($e, $b)
    $pull = Get-NsxService #| ?{$_.name -match "justin"}
    $index = 1
    $toprint = @()
    foreach($serv in $pull){
        Write-Host "[$($index)/$($pull.length)] Exporting $($serv.name)" -fore Cyan
        $whatup = New-Object serviceHelper
        $whatup.name = $serv.name
        $p = $serv | select -ExpandProperty element
        $whatup.protocol = $p.applicationprotocol
        $destPort = $p.value
        if(!$destPort){
            $whatup.destinationPort += "any"
        } else {
            $destPort = $destPort.split(",")
            $cd = 0
            foreach($x in $destPort){
                if($cd -eq 0){
                    $whatup.destinationPort += "$($x.tostring())"
                    $cd++
                } else {
                    $whatup.destinationPort += ", $($x.tostring())"
                }
            }
        }
        $sourcePort = $p.sourcePort
        if(!$sourcePort){
            $whatup.sourcePort += "any"
        } else {
            $sourcePort = $sourcePort.split(",")
            $cd = 0
            foreach($x in $sourcePort){
                if($cd -eq 0){
                    $whatup.sourcePort += "$($x.tostring())"
                    $cd++
                } else {
                    $whatup.sourcePort += ", $($x.tostring())"
                }
            }
        }
        
        $g = $serv | select -ExpandProperty scope
        $whatup.isuniversal = $g.id
        $toprint += $whatup
        $index++
    }
    $connected = $global:DefaultVIServer.Name.Split(".")
    $fileName = "$($e)b$($b)-services.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this one imports services from CSV takes a paramter to path of csv file to import
function importNSXServices{
    param($honda1)
    #$honda = Import-Csv C:\Users\IS_Admin\Documents\bunnyHopEnclave\b-services.csv
    $honda = Import-Csv $honda1 
    #"[$(Get-Date)] importing csv $($honda1)" | Out-File lumberjack.txt -Append
    #$honda.length
    $index = 1
    foreach($accord in $honda){
        Write-Host "[$($index)/$($honda.Length)] Importing $($accord.name)" -fore Cyan
        #"[$(Get-Date)] [$($index)/$($honda.Length)] Importing $($accord.name)" | Out-File lumberjack.txt -Append
        $accord.destinationPort = $accord.destinationPort -replace '\s', ''
        $accord.sourcePort = $accord.sourcePort -replace '\s',''

        if(($accord.destinationPort -eq "any") -and ($accord.sourcePort -eq "any")){
            try{
                $added = New-NsxService -Name $accord.name -Protocol $accord.protocol -scopeId $accord.isuniversal -EnableInheritance true
                Write-Host "created $($accord.name)" -fore Green
            } catch {
                Write-Host "$($accord.name) already exists, checking members" -fore Yellow
            }
        } elseif($accord.destinationPort -eq "any"){
            try{
                $added = New-NsxService -Name $accord.name -Protocol $accord.protocol -SourcePort $accord.sourcePort -scopeId $accord.isuniversal -EnableInheritance true
                Write-Host "created $($accord.name)" -fore Green
            } catch { 
                Write-Host "$($accord.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $accord.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $accord.protocol){
                    $srvPorts = $srvelem.sourceport.split(',')
                    $newPorts = $accord.sourceport.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($accord.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($accord.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($accord.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($accord.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($accord.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        } elseif($accord.sourcePort -eq "any"){
            try{
                $added = New-NsxService -Name $accord.name -Protocol $accord.protocol -port $accord.destinationPort -scopeId $accord.isuniversal -EnableInheritance true
                Write-Host "created $($accord.name)" -fore Green
            } catch {
                Write-Host "$($accord.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $accord.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $accord.protocol){
                    $srvPorts = $srvelem.value.split(',')
                    $newPorts = $accord.destinationPort.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($accord.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($accord.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($accord.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($accord.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($accord.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        } else {
            try{
                $added = New-NsxService -Name $accord.name -Protocol $accord.protocol -port $accord.destinationPort -SourcePort $accord.sourcePort -scopeId $accord.isuniversal -EnableInheritance true
                Write-Host "created $($accord.name)" -fore Green
            } catch {
                Write-Host "$($accord.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $accord.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $accord.protocol){
                    $srvPorts = $srvelem.sourceport.split(',')
                    $newPorts = $accord.sourceport.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($accord.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($accord.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($accord.name)" -fore Yellow
                        }
                    }
                    $srvPorts = $srvelem.value.split(',')
                    $newPorts = $accord.destinationPort.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($accord.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($accord.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($accord.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($accord.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($accord.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        }
        $index++
    }
}

#==============================================#
#NSX Grouping Objects - Service Groups         #
#==============================================#

#this function exports serviceGroups to CSV
function exportNSXServiceGroups{
    #$pull = Get-NsxServiceGroup -Name "justinroxgroup"
    [cmdletbinding()]
    param($e, $b)
    $pull = Get-NsxServiceGroup | ?{$_.isuniversal -eq "false"} #| ?{$_.name -match "justin"}
    $toprint = @()
    $index = 1
    foreach($serviceGroup in $pull){
        Write-Host "[$($index)/$($pull.length)] Exporting $($serviceGroup.name)" -fore Cyan
        $suh = New-Object serviceGroupHelper
        $suh.name = $serviceGroup.name
        $memz = $serviceGroup | select -ExpandProperty member -ErrorAction SilentlyContinue
        $count = 0
        foreach($m in $memz){
            if($count -eq 0){
                $suh.members += "$($m.name.tostring())&$($m.objecttypename.tostring())"
                $count++
            } else {
                $suh.members += ",$($m.name.tostring())&$($m.objecttypename.tostring())"
            }
        }
        $sco = $serviceGroup | select -ExpandProperty scope
        $suh.isUniversal = $sco.id
        $toprint += $suh
        $index++
    }
    $connected = $global:DefaultVIServer.Name.Split(".")
    $fileName = "$($e)b$($b)-serviceGroups.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this function imports serviceGroups from CSV
function importNSXServiceGroups{
    param($importpath)
    $audi = Import-Csv $importpath
    #$audi = Import-Csv C:\Users\IS_Admin\Documents\bunnyHopEnclave\b-serviceGroups.csv
    $index = 1
    foreach($r8 in $audi){
        Write-Host "[$($index)/$($audi.length)] importing service group $($r8.name)" -fore Cyan
        $serviceGroup = Get-NsxServiceGroup -Name "$($r8.name)" | ?{$_.isuniversal -eq "false"}
        if($serviceGroup -eq $null){
            Write-Host "service group does not exist, creating $($r8.name)" -fore green
            $serviceGroup = New-NsxServiceGroup -Name "$($r8.name)" -EnableInheritance true
        }
        $services = $r8.members -replace ', ', ','
        #$services = $r8.members
        $services = $services.split(",")
        if($services[0].length -gt 0){
            foreach($service in $services){
                $srv = $service.split("&")
                $srvName = $srv[0]
                $srvType = $srv[1]
                $serviceGroup = Get-NsxServiceGroup -Name "$($r8.name)" | ?{$_.isuniversal -eq "false"}
                if($Hostname-246 -contains $srvName){
                    Write-Host "$($serviceGroup.name) already contains $($srvName), move on" -fore Yellow
                } else {
                    if($srvType -eq "application"){
                        $service = Get-NsxService -Name "$($srvName)" | ?{$_.isuniversal -eq "false"}
                        $serviceGroup | Add-NsxServiceGroupMember -Member $service
                        Write-Host "adding service $($service.name)" -fore Green
                    } elseif($srvType -eq "applicationGroup") {
                        $service = Get-NsxServiceGroup -Name "$($srvName)" | ?{$_.isuniversal -eq "false"}
                        $serviceGroup | Add-NsxServiceGroupMember -Member $service
                        Write-Host "adding service $($service.name)" -fore Green
                    }
                } 
            }
        } 
        $index++
    }
}

#==============================================#
#NSX Security Tags                             #
#==============================================#

#this function exports security tags to csv
function exportNSXSecurityTags{
    [cmdletbinding()]
    param($e, $b)
    $pull = Get-NsxSecurityTag #| ?{$_.name -match "justin"}
    $toprint = @()
    $index = 1
    foreach($st in $pull){
        Write-Host "[$($index)/$($pull.length)] Exporting $($st.name)" -fore Cyan
        $suh = New-Object securityTagHelper
        $suh.name = $st.name
        $suh.description = $st.description
        $toprint += $suh
        $index++
    }
    $connected = $global:DefaultVIServer.Name.Split(".")
    $fileName = "$($e)b$($b)-securitytags.csv"
    #$toprint | Export-Csv $fileName -NoTypeInformation -Append
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this function imports security tags to csv
function importNSXSecurityTags{
    param($importpath)
    $nissan = Import-Csv $importpath
    $index = 1
    foreach($gtr in $nissan){
        Write-Host "[$($index)/$($nissan.length)] importing security tag $($gtr.name)" -fore Cyan
        if((Get-NsxSecurityTag -Name $gtr.name).name -contains $gtr.name){
            "[$(Get-Date)] nsx security tag $($gtr.name) already exists, moving on" | Out-File manualOverride.txt -Append
            Write-Host "nsx security tag $($gtr.name) already exists, moving on" -fore Yellow
        } else {
            $tg = New-NsxSecurityTag -Name $gtr.name -Description $gtr.description
            if($tg){
                #"[$(Get-Date)] successfully created security tag $($gtr.name)" | Out-File manualOverride.txt -Append
                Write-Host "successfully created security tag $($gtr.name)" -fore Green
            } else {
                "[$(Get-Date)] manual input of $($gtr.name) required, creation of security tag failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($gtr.name) required, creation of security tag failed" -fore Red
            }
        }
        $index++
    }
}

#==============================================#
#Cleanup                                       #
#==============================================#

#returns an array of security groups and ispets that are in use for that main security group
#three functions work in conjunction
function getAllMembers{
    param($in)
    $retVal = @()
    if(!($in.member)){
        #Write-Host "no members, return" -fore Yellow
        return $in
    } else {
        foreach($member in $in.member){
        $saveMem = $member
        #Write-Host "just got $($member.name) with members $($member.member)"
        try{
            $member = Get-NsxSecurityGroup -objectId $member.objectid
            #Write-Host "got security group" -fore Yellow
        }catch{
            $member = $saveMem
            #Write-Host "$($in.name) is not a security group"
        }
        getAllMembers $member
        }  
    }
    #Write-Host "final return" -fore Yellow
    $retVal += $in
    return $retVal
}

function getNestedSG{
    param($baseSG)
    #$baseSG
    #Write-Host "getting base nsx security group $($baseSG)" -fore Yellow
    $sg = Get-NsxSecurityGroup -name $baseSG
    #Write-Host "object retrieved $($sg.name)"
    $ret = getAllMembers $sg
    #Write-Host "should return banana $($ret.name)"
    $toReturn = @()
    foreach($reet in $ret){
        if($toReturn.objectid -notcontains $reet.objectid){
            $toReturn += $reet
        }
    }
    $toReturn | Export-Csv jkt2.csv -NoTypeInformation
    return $toReturn
}

function ipsToClean{
    param($obj)
    #Write-Host "Gathering all IPSets that are currently not in use" -fore Cyan
    $allIPS = Get-NsxFirewallRule
    #Write-Host "Gathering all sources and destinations" -fore Cyan
    $srcs = $allIPS | select name, sources
    $allSrcandDest = @()
    $progressSRC = 0
    $denomSRC = ($srcs.Length + 1)*100
    foreach($src in $srcs){
        Write-Host "Gathering sources for rule $($src.name)" -fore Green
        $s = $src.sources.source
        foreach($sa in $s){
            Write-Host "Gathering contents of source $($sa.name)"
            if($sa.type -eq "securitygroup"){
                #Write-Host "is a security group so about to nest" -fore Yellow
                $allSrcandDest += (getNestedSG $sa.name)
            } elseif($sa.type -eq "ipset"){
                #Write-Host "is an ipset so getting the ipset" -fore Yellow
                $allSrcandDest += (Get-NsxIpSet -objectId $sa.value)
            }
        }
        $progressSRC++
        try{
            #Write-Host "$($progressSRC) / $($srcs.Length*100)"
            Write-Progress -Activity "Getting Sources" -Status "Checking $($src.name)" ` -PercentComplete($progressSRC/($srcs.Length)*100)
        }catch{
            Write-Host "completed sources"
        }
        
    }

    $dsts = $allIPS | select name, destinations
    $progressDST = 0
    $denomDST = ($dsts.Length + 1)*100
    foreach($dst in $dsts){
        Write-Host "Gathering destinations for rule $($dst.name)" -fore Green
        $s = $dst.destinations.destination
        foreach($sa in $s){
            Write-Host "Gathering contents of destination $($sa.name)"
            if($sa.type -eq "securitygroup"){
                $allSrcandDest += (getNestedSG $sa.name)
            } elseif($sa.type -eq "ipset"){
                $allSrcandDest += (Get-NsxIpSet -objectId $sa.value)
            }
        }
        $progressDST++
        try{
            #Write-Host "$($progressDST) / $($dsts.Length*100)"
            Write-Progress -Activity "Getting Destinations" -Status "Checking $($dst.name)" ` -PercentComplete($progressDST/($dsts.Length)*100)
        }catch{
            Write-Host "completed destinations"
        }
        
    }

    $delete = @()
    if($obj -eq "ip"){
        $erryThing = Get-NsxIpSet
        $masterIP = $allSrcandDest | ?{$_.objecttypename -eq "ipset"}
        foreach($reet in $erryThing){
            if($masterIP.objectid -notcontains $reet.objectid){
                $delete += $reet
            }
        }
        #$delete is a list of all ipsets that are not inuse in any firewall rule currently
    } elseif($obj -eq "sg"){
        $erryThing = Get-NsxSecurityGroup
        $masterIP = $allSrcandDest | ?{$_.objecttypename -eq "securitygroup"}
        foreach($reet in $erryThing){
            if($masterIP.objectid -notcontains $reet.objectid){
                $delete += $reet
            }
        }
        #$delete is a list of all sercurityGroups that are not inuse in any firewall rule currently
    }
    
    
    return $delete
}

function searchanddestroyIPSETS{
    Write-Host "Searching and Destroy IPSets" -fore Cyan
    $date1 = $(Get-Date)
    Write-Host "$($date1)" -fore Yellow
    $delete = ipsToClean "ip"
    $date2 = $(Get-Date)
    Write-Host "$($date2)" -fore Yellow
    $time = $date2 - $date1
    Write-Host "total time to complete $($time)" -fore Yellow
    $delete.length
    $delete | Export-Csv ipsetsToDestroy.csv -NoClobber
    <#$delete = Import-Csv C:\Users\IS_Admin\Documents\bunnyHopEnclave\rb2\cleanup\ipsetsToDestroy.csv
    foreach($del in $delete){
        Write-Host "DESTROYING $($del.name)" -fore Green
        Remove-NsxIpSet -IPSet (Get-NsxIpSet -objectId $del.objectId) -Confirm:$false
        #Remove-NsxIpSet -IPSet $del -Confirm:$false
        Write-Host "DESTROYED $($del.name)" -fore Green
    }#>
}

function seekanddestroySECURITYGROUPS{
    Write-Host "searching and destroying security groups" -fore Cyan
    $date1 = $(Get-Date)
    Write-Host "$($date1)" -fore Yellow
    $delete = ipsToClean "sg"
    $date2 = $(Get-Date)
    Write-Host "$($date2)" -fore Yellow
    $time = $date2 - $date1
    Write-Host "total time to complete $($time)" -fore Yellow
    $delete.length
    $delete | Export-Csv sgToDestroy.csv -NoClobber
    <#$delete = Import-Csv C:\Users\IS_Admin\Documents\bunnyHopEnclave\rb2\cleanup\sgToDestroy.csv
    foreach($del in $delete){
        Write-Host "DESTROYING $($del.name)" -fore Green
        #Remove-NsxSecurityGroup -SecurityGroup $del 
        Remove-NsxSecurityGroup -SecurityGroup (Get-NsxSecurityGroup -objectId $del.objectId) -confirm:$false 
        Write-Host "DESTROYED $($del.name)" -fore Green
    }#>
}

#==============================================#
#Scratch                                       #
#==============================================#

function asciiArt{
    clear

}

#==============================================#
#runOnImport                                   #
#==============================================#







