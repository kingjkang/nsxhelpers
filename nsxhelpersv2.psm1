#==============================================#
#Environment Variables                         #
#==============================================#
$usernamewithoutdomain = "administratorAccount"
$domain = "Hostname-56"
$creds = Get-Credential -Message "Store user/admin credentials for VCSA" -UserName "$($usernamewithoutdomain)@$($domain)"
$nsxCreds = Get-Credential -Message "Store user/admin credentials for NSXmgr" -UserName "$($usernamewithoutdomain)@$($domain)"
$vcenter = "vcsa"
$nsxmgr = "nsx manager"

#==============================================#
#Required and Recommended Modules              #
#==============================================#
dir  C:\Users\Documents\ps-master\=.= | Unblock-File
Import-Module [path]\PowerNSX.psd1
Import-Module [path]\PowerNSX.psm1
Import-Module VMware.VimAutomation.Core
Import-Module VMware.VimAutomation.Vds
Import-Module VMware.VimAutomation.Storage

#==============================================#
#Required Classes                              #
#==============================================#

Class firewallHelper{
    #properties
    [String] $name
    [String] $source
    [String] $destination
    [String] $service
    [String] $section
    [String] $action
}

#this class needs to be imported when working with service functions
Class serviceHelper{
    #properties
    [String] $name
    [String] $protocol
    [String] $destinationPort
    [String] $sourcePort
    [String] $isuniversal

}

#this class needs to be imported when working with service functions
Class serviceGroupHelper{
    #properties
    [String] $name
    [String] $members
    [String] $isUniversal
}

#this class needs to be imported when working with ipsets functions
Class IPSetHelper{
    #properties
    [String] $name
    [String] $members
    [String] $isUniversal
}

#this class needs to be imported when working with securitygroups functions
Class DynamicSecurityGroupHelper{
    #properties
    [String] $setOperator
    [String] $criteriaOperator
    [String] $key
    [String] $criteria
    [String] $value
}

#this class needs to be imported when working with securitygroups functions
Class SecurityGroupHelper{
    #properties
    [String] $name
    [System.Object] $dynamicMembership
    [System.Object] $staticInclude
    [System.Object] $staticExclude
}

#this class needs to be imported when working with securitytags functions
Class securityTagHelper{
    #properties
    [String] $name
    [String] $description
}

#==============================================#
#NSX Objects ALL                               #
#==============================================#

#==============================================#
#Distributed Firewalls                         #
#==============================================#
#if a section is specified then the entire section will be exported
#if a ruleid is specified then that rule will be exported
#if neither of the two are specified then the entire nsx mgr will be exported
function exportNSXFirewallConfiguration{
    [cmdletbinding()]
    param($ruleid, $section, $fileName)
    
    #checking for correct vcenter connection
    if($global:DefaultVIServers){
        Write-Host "you are currently connected to one or more vcenters"
        Write-Host "$(($global:DefaultVIServers).name)" -fore Green
        $response = Read-Host "if you are connected to the correct vcenter enter y or yes else enter n or no"
        if(($response -eq "n") -or ($response -eq "no")){
            Disconnect-VIServer * -Confirm:$false -WarningAction SilentlyContinue
            $vctoconn = Read-Host "enter vcsa or ipaddress to connect to"
            Connect-VIServer $vctoconn -warningaction silentlycontinue
        }
    } else {
        $vctoconn = Read-Host "enter vcsa or ipaddress to connect to"
        Connect-VIServer $vctoconn -warningaction silentlycontinue
    }

    #checking for correct nsx manager connection
    if($defaultnsxconnection){
        Write-Host "you are currently connected to one or more nsx managers"
        Write-Host "$(($defaultnsxconnection).server)" -fore Green
        $response = Read-Host "if you are connected to the correct nsxmgr enter y or yes else enter n or no"
        if(($response -eq "n") -or ($response -eq "no")){
            Disconnect-NsxServer * -confirm:$false -warningaction silentlycontinue
            $nsxtoconn = Read-Host "enter nsx mgr or ipaddress to connect to"
            Connect-NsxServer $nsxtoconn -warningaction silentlycontinue
        }
    } else {
        $nsxtoconn = Read-Host "enter nsx mgr or ipaddress to connect to"
        Connect-NsxServer $nsxtoconn -warningaction silentlycontinue
    }

    #checking to see if user wants to continue
    Write-Host "PLEASE CHECK PWD" -fore Yellow
    $cont = Read-Host "If you would like to continue enter y or yes, if you do not enter n or no and run function again"
    if(($cont -eq "n") -or ($cont -eq "no")){
        break
    }

    #filtering by provided parameter
    if($ruleid){
        $rules = Get-NsxFirewallRule -RuleId $ruleid
    } elseif($section) {
        $rules = Get-NsxFirewallRule -Section (Get-NsxFirewallSection -Name $section)
    } else {
        $rules = Get-NsxFirewallRule
    }
    
    #beginning the export part
    Write-Host "Starting exporting NSX Firewall Rules" -fore Green
    $ruleData = $rules | select name, sources, destinations, services, action, sectionid
    $toprint = @()
    $index = 1

    foreach($rule in $ruleData){
        Write-Host "[$($index)/$($ruleData.length)] Exporting $($rule.name)" -fore Cyan
        $ruleToExport = New-Object firewallHelper
        $ruleToExport.name = $rule.name

        $src = $rule.sources | select -ExpandProperty source
        $srcCount = 0
        if($src.Length -eq 0){
            $ruleToExport.source = "any"
        } else {
            foreach($currSrc in $src){
                if((($currSrc.value).split("-"))[0] -eq "ipset"){
                    $srcScope = (Get-NsxIpSet -objectId ($currSrc.value)).scope.id
                } elseif((($currSrc.value).split("-"))[0] -eq "securitygroup"){
                    $srcScope = (Get-NsxSecurityGroup -objectId ($currSrc.value)).scope.id
                } else {
                    $srcScope = "n/a"
                }

                if($srcCount -eq 0){
                    if(($currSrc.type -eq "ipv4address") -or ($currSrc.type -eq "ipv6address")){
                        $ruleToExport.source += "$($currSrc.value)&$($currSrc.type)&$($srcScope)"
                        $srcCount++
                    } else {
                        $ruleToExport.source += "$($currSrc.name)&$($currSrc.type)&$($srcScope)"
                        $srcCount++
                    }
                } else {
                    if(($currSrc.type -eq "ipv4address") -or ($currSrc.type -eq "ipv6address")){
                        $ruleToExport.source += ",$($currSrc.value)&$($currSrc.type)&$($srcScope)"
                    } else {
                        $ruleToExport.source += ",$($currSrc.name)&$($currSrc.type)&$($srcScope)"
                    }
                }
            }
        }

        $dst = $rule.destinations | select -ExpandProperty destination
        $dstCount = 0
        if($dst.length -eq 0){
            $ruleToExport.destination = "any"
        } else {
            foreach($currDst in $dst){
                if((($currDst.value).split("-"))[0] -eq "ipset"){
                    $dstScope = (Get-NsxIpSet -objectId ($currDst.value)).scope.id
                } elseif((($currDst.value).split("-"))[0] -eq "securitygroup"){
                    $dstScope = (Get-NsxSecurityGroup -objectId ($currDst.value)).scope.id
                } else {
                    $dstScope = "n/a"
                }

                if($dstCount -eq 0){
                    if(($currDst.type -eq "ipv4address") -or ($currDst.type -eq "ipv6address")){
                        $ruleToExport.destination += "$($currDst.value)&$($currDst.type)&$($dstScope)"
                        $dstCount++
                    } else {
                        $ruleToExport.destination += "$($currDst.name)&$($currDst.type)&$($dstScope)"
                        $dstCount++
                    }
                } else {
                    if(($currDst.type -eq "ipv4address") -or ($currDst.type -eq "ipv6address")){
                        $ruleToExport.destination += ",$($currDst.value)&$($currDst.type)&$($dstScope)"
                    } else {
                        $ruleToExport.destination += ",$($currDst.name)&$($currDst.type)&$($dstScope)"
                    }
                }
            }   
        }

        $srv = $rule.services | select -ExpandProperty service
        $srvCount = 0
        if($srv.length -eq 0){
            $ruleToExport.service = "any"
        } else {
            foreach($currSrv in $srv){
                if((($currSrv.value).split("-"))[0] -eq "application"){
                    $srvScope = (Get-NsxService -objectId ($currSrv.value)).scope.id
                } elseif((($currSrv.value).split("-"))[0] -eq "applicationgroup"){
                    $srvScope = (Get-NsxServiceGroup -objectId ($currSrv.value)).scope.id
                } else {
                    $srvScope = "n/a"
                }

                if($srvCount -eq 0){
                    $ruleToExport.service += "$($currSrv.name)&$($currSrv.type)&$($srvScope)"
                    $srvCount++
                } else {
                    $ruleToExport.service += ",$($currSrv.name)&$($currSrv.type)&$($srvScope)"
                }
            }
        }

        $ruleToExport.section = (Get-NsxFirewallSection -objectId $rule.sectionid).name

        $ruleToExport.action = $rule.action
        
        $toprint += $ruleToExport
        $index++
    }

    if($fileName){
        $fileName = "$($fileName)-DFWR.csv"
    } else {
        $fileName = "$($defaultnsxconnection.Server)-DFWR-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation

    $pwdpath = (pwd).Path
    $filepath = $pwdpath + "\$($fileName)"
    $dfwrImport = Import-Csv $filepath
    Write-Host "Finished exporting NSX Firewall Rules" -fore Green

    #securitytags
    Write-Host "Starting exporting NSX Security tags" -fore Green
    exportNSXSecurityTags -fileName $fileName
    Write-Host "Finished exporting NSX Security tags" -fore Green

    #services and service groups
    $servicesImport = $dfwrImport.service
    $servicesImport = $servicesImport.split(",")
    $servicesToExport = @()
    $serviceGroupsToExport = @()
    foreach($item in $servicesImport){
        if(($item.split("&"))[1] -eq "Application"){
            $servicesToExport += ($item.split("&"))[0]
        } elseif(($item.split("&"))[1] -eq "ApplicationGroup"){
            $serviceGroupsToExport += ($item.split("&"))[0]
        }
    }
    Write-Host "Starting Exporting NSX Services" -fore Green
    exportNSXServices -fileName $fileName -serviceNames $servicesToExport
    Write-Host "Finished exporting NSX Services" -fore Green
    
    Write-Host "Starting exporting NSX Service Groups" -fore Green
    exportNSXServiceGroups -fileName $fileName -serviceGroupNames $serviceGroupsToExport
    Write-Host "Finished exporting NSX service groups" -fore Green
    
    #ipsets & securitygroups
    $dstImport = $dfwrImport.destination
    $dstImport = $dstImport.split(",")
    $srcImport = $dfwrImport.source
    $srcImport = $srcImport.split(",")
    $ipsetsToExport = @()
    $securityGroupsToExport = @()
    $objToExport = $dstImport + $srcImport
    foreach($item in $objToExport){
        if(($item.split("&"))[1] -eq "IPSet"){
            $ipsetsToExport += ($item.split("&"))[0]
        } elseif(($item.split("&"))[1] -eq "SecurityGroup"){
            $securityGroupsToExport += ($item.split("&"))[0]
        }
    }
    $ipsetsToExport = $ipsetsToExport | select -Unique
    $securityGroupsToExport = $securityGroupsToExport | select -Unique
    Write-Host "Starting exporting NSX IPSets" -fore Green
    exportNSXIPSets -fileName $fileName -IPSetNames $ipsetsToExport
    Write-Host "Finished exporting NSX IPSets" -fore Green

    Write-Host "Starting exporting NSX Security Groups" -fore Green
    exportNSXSecurityGroups -filename $fileName -SecurityGroupNames $securityGroupsToExport
    Write-Host "Finished exporting NSX Security Groups" -fore Green

}

function importNSXFirewallRules{
    param($importpath)
    $distributedFirewallRules = Import-Csv $importpath
    $index = 1

    foreach($camry in $distributedFirewallRules){
        Write-Host "[$($index)/$($toyota.length)] importing $($rav4.name)" -fore Cyan
        $iName = $distributedFirewallRule.name
        $iSource = $distributedFirewallRule.source
        $iDestination = $distributedFirewallRule.destination
        $iService = $distributedFirewallRule.service
        $iSection = $distributedFirewallRule.section
        $iAction = $distributedFirewallRule.action

        Write-Host "checking the firewall section $($iSection)" -fore Cyan
        if((Get-NsxFirewallSection -Name $iSection).name -notcontains $iSection){
            if((Get-NsxFirewallSection -Name $iSection) -match "::"){
                "[$(Get-Date)] manual input of $($iSection) needed, addition of firewall policy failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($iSection) needed, addition of firewall policy failed" -fore Red
            } else {
                $defSec = Get-NsxFirewallSection
                $defSecID = $defSec[$defSec.length-1]
                $nSection = New-NsxFirewallSection -Name $iSection -scopeId globalroot-0 -sectionType layer3sections -position before $defSecID.id 
                $nSection = Get-NsxFirewallSection -Name $iSection
            }
        } else {
            $nSection = Get-NsxFirewallSection -Name $iSection
        }

        Write-Host "checking the firewall rule members" -fore Cyan
        $controlset = (Get-NsxFirewallRule -Name $iName).name
        if($controlset -contains $iName){
            Write-Host "the firewall rule $($iname) already exists so checking and adding the src, dst, serv" -fore Yellow
            #check the sources 
            $listofsources = $iSource.split(",")
            $nSource = @() 
            foreach($srcinlist in $listofsources){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $srcinlist.Split("&")
                if($temp[1] -eq "IPSet"){
                    $setoctrl = ($controlset.sources.source | ?{$_.type -eq "ipset"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) source required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) sources" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Source -Member (Get-NsxIpSet -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "SecurityGroup"){
                    $setoctrl = ($controlset.sources.source | ?{$_.type -eq "securitygroup"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) source required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) sources" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Source -Member (Get-NsxSecurityGroup -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }
            #check the destinations
            $listofdestinations = $iDestination.split(",")
            $nDestination = @() 
            foreach($dstinlist in $listofdestinations){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $dstinlist.Split("&")
                if($temp[1] -eq "IPSet"){
                    $setoctrl = ($controlset.destinations.destination | ?{$_.type -eq "ipset"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) destination required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) destinations" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Destination -Member (Get-NsxIpSet -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "SecurityGroup"){
                    $setoctrl = ($controlset.destinations.destination | ?{$_.type -eq "securitygroup"}).name
                    if($setoctrl -contains $temp[0]){
                        Write-Host "$($temp[0]) already exists so check the value" -fore Yellow
                        "[$(Get-Date)] manual validation of $($temp[0]) for rule $($iName) destination required" | Out-File manualOverride.txt -Append
                    } else {
                        Write-Host "adding $($temp[0]) to rule $($iName) destinations" -fore Green
                        $toa = Add-NsxFirewallRuleMember -FirewallRule $controlset -MemberType Destination -Member (Get-NsxSecurityGroup -Name $temp[0])
                        if($toa){
                            Write-Host "added nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Green
                        } else {
                            Write-Host "failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" -fore Red
                            "[$(Get-Date)] failed to add nsx firewall rule member $($temp[0]) to rule $($iName)" | Out-File manualOverride.txt -Append
                        }
                    }
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }
            #check the services
            $listofservices = $iService.split(",")
            $nService = @()
            foreach($srvinlist in $listofservices){
                $controlset = Get-NsxFirewallRule -Name $iName
                $temp = $srvinlist.Split("&")
                if($temp[1] -eq "application"){
                    $setocontrol = ($controlset.services.service | ?{$_.type -eq "application"}).name
                    if($setocontrol -contains $temp[0]){
                        Write-Host "firewall rule $($iName) already contains $($temp[0])" -fore Yellow
                    } else {
                        "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                         Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                    }
                } elseif($temp[1] -eq "applicationgroup"){
                    $setocontrol = ($controlset.services.service | ?{$_.type -eq "applicationgroup"}).name
                    if($setocontrol -contains $temp[0]){
                        Write-Host "firewall rule $($iName) already contains $($temp[0])" -fore Yellow
                    } else {
                        "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                         Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                    }
                } else {
                    "[$(Get-Date)] firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" | Out-File manualOverride.txt -Append
                    Write-Host "firewall rule $($iName) does not contains $($temp[0]), requires manual intervention" -fore Red
                }
            }
        } else {
            Write-Host "the firewall rule $($iname) does not already exist so creating the objects and creating new firewall rule" -fore Green
            $listofsources = $iSource.split(",")
            $nSource = @() 
            foreach($srcinlist in $listofsources){
                $temp = $srcinlist.Split("&")
                if($temp[1] -eq "IPSet"){
                    $nSource += Get-NsxIpSet -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "SecurityGroup"){
                    $nSource += Get-NsxSecurityGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }

            $listofdestinations = $iDestination.split(",")
            $nDestination = @()
            foreach($dstinlist in $listofdestinations){
                $temp = $dstinlist.Split("&")
                if($temp[1] -eq "IPSet"){
                    $nDestination += Get-NsxIpSet -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "SecurityGroup"){
                    $nDestination += Get-NsxSecurityGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($temp[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of cluster failed" -fore Red
                } elseif($temp[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName.name) needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($temp[0]) to $($iName) needed, addition of $($temp[1]) failed" -fore Red
                }
            }

            $listofservices = $iService.split(",")
            $nService = @()
            foreach($srvinlist in $listofservices){
                $temp = $srvinlist.Split("&")
                if($temp[1] -eq "Application"){
                    $nService += Get-NsxService -Name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                } elseif($temp[1] -eq "ApplicationGroup"){
                    $nService += Get-NsxServiceGroup -name $temp[0] | Where-Object{$_.isUniversal -eq "false"}
                }
            }

            try{

                if(($nSource -eq "any") -and ($nDestination -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any") -and ($nDestination -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Destination $nDestination -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nSource -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Destination $nDestination -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nDestination -eq "any") -and ($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nDestination -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Service $nService -Action $iAction -EnableLogging -Position Bottom
                }elseif(($nService -eq "any")){
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Destination $nDestination -Action $iAction -EnableLogging -Position Bottom
                }else{
                    $completed = New-NsxFirewallRule -Name $iName -Section $nSection -Source $nSource -Destination $nDestination -Service $nService -Action $iAction -EnableLogging -Position Bottom -Disabled
                }

            } catch {
                $handle = $false
            } finally {
                if($handle -eq $false){
                    Write-Output "Creation of rule $($camry.name) failed"
                } else {
                    Write-Host "Creation of rule $($camry.name) succeeded" -fore Cyan
                }
            }
        }

        $index++
    }
}
#==============================================#
#Security Tags                                 #
#==============================================#
#this function exports security tags to csv
#if an array of tag names is passed it will only export those
#if a file name is provided that will be the name of the file path
function exportNSXSecurityTags{
    [cmdletbinding()]
    param($tagNames, $fileName)
    $secTags = @()
    if($tagNames){
        foreach($tag in $tagNames){
            $secTags += Get-NsxSecurityTag -Name $tag
        }
    } else {
        $secTags = Get-NsxSecurityTag
    }

    $toprint = @()
    $index = 1
    foreach($st in $secTags){
        Write-Host "[$($index)/$($secTags.length)] Exporting $($st.name)" -fore Cyan
        $secTagToExport = New-Object securityTagHelper
        $secTagToExport.name = $st.name
        $secTagToExport.description = $st.description
        $toprint += $secTagToExport
        $index++
    }

    if($fileName){
        $fileName = $fileName.Replace("DFWR", "SecurityTags")
    } else {
        $fileName = "$($defaultnsxconnection.Server)-SecurityTags-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this function imports security tags from csv
function importNSXSecurityTags{
    param($importpath)
    $secTags = Import-Csv $importpath
    $index = 1
    foreach($st in $secTags){
        Write-Host "[$($index)/$($secTags.length)] importing security tag $($st.name)" -fore Cyan
        if((Get-NsxSecurityTag -Name $st.name).name -contains $st.name){
            "[$(Get-Date)] nsx security tag $($st.name) already exists, moving on" | Out-File manualOverride.txt -Append
            Write-Host "nsx security tag $($st.name) already exists, moving on" -fore Yellow
        } else {
            $tg = New-NsxSecurityTag -Name $st.name -Description $st.description
            if($tg){
                #"[$(Get-Date)] successfully created security tag $($gtr.name)" | Out-File manualOverride.txt -Append
                Write-Host "successfully created security tag $($st.name)" -fore Green
            } else {
                "[$(Get-Date)] manual input of $($st.name) required, creation of security tag failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($st.name) required, creation of security tag failed" -fore Red
            }
        }
        $index++
    }
}

#==============================================#
#Services                                      #
#==============================================#
#this function exports services to csv
function exportNSXServices{
    [cmdletbinding()]
    param($serviceNames, $fileName)
    $services = @()
    if($serviceNames){
        foreach($srv in $serviceNames){
            $services += Get-NsxService -Name $srv
        }
    } else {
        $services = Get-NsxService
    }

    $index = 1
    $toprint = @()
    foreach($serv in $services){
        Write-Host "[$($index)/$($services.length)] Exporting $($serv.name)" -fore Cyan
        $servicesToExport = New-Object serviceHelper
        $servicesToExport.name = $serv.name
        $p = $serv | select -ExpandProperty element
        $servicesToExport.protocol = $p.applicationprotocol
        $destPort = $p.value
        if(!$destPort){
            $servicesToExport.destinationPort += "any"
        } else {
            $destPort = $destPort.split(",")
            $cd = 0
            foreach($x in $destPort){
                if($cd -eq 0){
                    $servicesToExport.destinationPort += "$($x.tostring())"
                    $cd++
                } else {
                    $servicesToExport.destinationPort += ", $($x.tostring())"
                }
            }
        }
        $sourcePort = $p.sourcePort
        if(!$sourcePort){
            $servicesToExport.sourcePort += "any"
        } else {
            $sourcePort = $sourcePort.split(",")
            $cd = 0
            foreach($x in $sourcePort){
                if($cd -eq 0){
                    $servicesToExport.sourcePort += "$($x.tostring())"
                    $cd++
                } else {
                    $servicesToExport.sourcePort += ", $($x.tostring())"
                }
            }
        }
        
        $g = $serv | select -ExpandProperty scope
        $servicesToExport.isuniversal = $g.id
        $toprint += $servicesToExport
        $index++
    }

    if($fileName){
        $fileName = $fileName.Replace("DFWR", "Services")
    } else {
        $fileName = "$($defaultnsxconnection.Server)-Services-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this one imports services from CSV takes a paramter to path of csv file to import
function importNSXServices{
    param($importpath)
    $servicesToImport = Import-Csv $importpath 
    $index = 1
    foreach($service in $servicesToImport){
        Write-Host "[$($index)/$($servicesToImport.Length)] Importing $($service.name)" -fore Cyan
        $service.destinationPort = $service.destinationPort -replace '\s', ''
        $service.sourcePort = $service.sourcePort -replace '\s',''

        if(($service.destinationPort -eq "any") -and ($service.sourcePort -eq "any")){
            try{
                $added = New-NsxService -Name $service.name -Protocol $service.protocol -scopeId $service.isuniversal -EnableInheritance true
                Write-Host "created $($service.name)" -fore Green
            } catch {
                Write-Host "$($service.name) already exists, checking members" -fore Yellow
            }
        } elseif($service.destinationPort -eq "any"){
            try{
                $added = New-NsxService -Name $service.name -Protocol $service.protocol -SourcePort $service.sourcePort -scopeId $service.isuniversal -EnableInheritance true
                Write-Host "created $($service.name)" -fore Green
            } catch { 
                Write-Host "$($service.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $service.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $service.protocol){
                    $srvPorts = $srvelem.sourceport.split(',')
                    $newPorts = $service.sourceport.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($service.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($service.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($service.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($service.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($service.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        } elseif($service.sourcePort -eq "any"){
            try{
                $added = New-NsxService -Name $service.name -Protocol $service.protocol -port $service.destinationPort -scopeId $service.isuniversal -EnableInheritance true
                Write-Host "created $($service.name)" -fore Green
            } catch {
                Write-Host "$($service.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $service.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $service.protocol){
                    $srvPorts = $srvelem.value.split(',')
                    $newPorts = $service.destinationPort.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($service.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($service.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($service.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($service.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($service.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        } else {
            try{
                $added = New-NsxService -Name $service.name -Protocol $service.protocol -port $service.destinationPort -SourcePort $service.sourcePort -scopeId $service.isuniversal -EnableInheritance true
                Write-Host "created $($service.name)" -fore Green
            } catch {
                Write-Host "$($service.name) already exists, checking members" -fore Yellow
                $srv = Get-NsxService -Name $service.name
                $srvelem = $srv.element
                if($srvelem.applicationprotocol -eq $service.protocol){
                    $srvPorts = $srvelem.sourceport.split(',')
                    $newPorts = $service.sourceport.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($service.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($service.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($service.name)" -fore Yellow
                        }
                    }
                    $srvPorts = $srvelem.value.split(',')
                    $newPorts = $service.destinationPort.split(',')
                    foreach($prt in $newPorts){
                        if(!($srvPorts -contains $prt)){
                            Write-Host "manual input of port $($prt) to service $($service.name) required" -fore Red
                            "[$(Get-Date)] manual input of port $($prt) to service $($service.name) required" | Out-File manualOverride.txt -Append
                            $srvPorts += $prt
                        } else {
                            Write-Host "port $($prt) already exists in $($service.name)" -fore Yellow
                        }
                    }
                } else {
                    Write-Host "manual input of service $($service.name) required, the protocol is different" -fore Red
                    "[$(Get-Date)] manual input of service $($service.name) required, the protocol is different" | Out-File manualOverride.txt -Append
                }
            }
        }
        $index++
    }
}

#==============================================#
#Service Groups                                #
#==============================================#
#this function exports serviceGroups to CSV
function exportNSXServiceGroups{
    [cmdletbinding()]
    param($serviceGroupNames, $fileName)
    $serviceGroups = @()
    if($serviceGroupNames){
        foreach($srvGrp in $serviceGroupNames){
            $serviceGroups += Get-NsxServiceGroup -Name $srvGrp
        }
    } else {
        $serviceGroups = Get-NsxServiceGroup
    }

    $toprint = @()
    $index = 1
    foreach($serviceGroup in $serviceGroups){
        Write-Host "[$($index)/$($serviceGroups.length)] Exporting $($serviceGroup.name)" -fore Cyan
        $serviceGroupsToExport = New-Object serviceGroupHelper
        $serviceGroupsToExport.name = $serviceGroup.name
        $memz = $serviceGroup | select -ExpandProperty member -ErrorAction SilentlyContinue
        $count = 0
        foreach($m in $memz){
            if($count -eq 0){
                $serviceGroupsToExport.members += "$($m.name.tostring())&$($m.objecttypename.tostring())"
                $count++
            } else {
                $serviceGroupsToExport.members += ",$($m.name.tostring())&$($m.objecttypename.tostring())"
            }
        }
        $sco = $serviceGroup | select -ExpandProperty scope
        $serviceGroupsToExport.isUniversal = $sco.id
        $toprint += $serviceGroupsToExport
        $index++
    }
    
    if($fileName){
        $fileName = $fileName.Replace("DFWR", "ServiceGroups")
    } else {
        $fileName = "$($defaultnsxconnection.Server)-ServiceGroups-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this function imports serviceGroups from CSV
function importNSXServiceGroups{
    param($importpath)
    $serviceGroupsToImport = Import-Csv $importpath
    $index = 1
    foreach($sgimporting in $serviceGroupsToImport){
        Write-Host "[$($index)/$($serviceGroupsToImport.length)] importing service group $($sgimporting.name)" -fore Cyan
        $serviceGroup = Get-NsxServiceGroup -Name "$($sgimporting.name)" | ?{$_.isuniversal -eq "false"}
        if($serviceGroup -eq $null){
            Write-Host "service group does not exist, creating $($sgimporting.name)" -fore green
            $serviceGroup = New-NsxServiceGroup -Name "$($sgimporting.name)" -EnableInheritance true
        }
        $services = $sgimporting.members -replace ', ', ','
        $services = $services.split(",")
        if($services[0].length -gt 0){
            foreach($service in $services){
                $srv = $service.split("&")
                $srvName = $srv[0]
                $srvType = $srv[1]
                $serviceGroup = Get-NsxServiceGroup -Name "$($sgimporting.name)" | ?{$_.isuniversal -eq "false"}
                if($serviceGroup.member.name -contains $srvName){
                    Write-Host "$($serviceGroup.name) already contains $($srvName), move on" -fore Yellow
                } else {
                    if($srvType -eq "application"){
                        $service = Get-NsxService -Name "$($srvName)" | ?{$_.isuniversal -eq "false"}
                        $serviceGroup | Add-NsxServiceGroupMember -Member $service
                        Write-Host "adding service $($service.name)" -fore Green
                    } elseif($srvType -eq "applicationGroup") {
                        $service = Get-NsxServiceGroup -Name "$($srvName)" | ?{$_.isuniversal -eq "false"}
                        $serviceGroup | Add-NsxServiceGroupMember -Member $service
                        Write-Host "adding service $($service.name)" -fore Green
                    }
                } 
            }
        } 
        $index++
    }
}

#==============================================#
#IPSets                                        #
#==============================================#
#export ipset to csv
function exportNSXIPSets{
    [cmdletbinding()]
    param($IPSetNames, $fileName)
    $IPSets = @()
    if($IPSetNames){
        foreach($ips in $IPSetNames){
            $IPSets += Get-NsxIpSet -Name $ips
        }
    } else {
        $IPSets = Get-NsxIpSet
    }

    $index = 1
    $toprint = @()
    foreach($ipset in $IPSets){
        Write-Host "[$($index)/$($IPSets.length)] Exporting $($ipset.name)" -fore Cyan
        $ipsetToExport = New-Object IPSetHelper
        $ipsetToExport.name = $ipset.name
        $ipsetToExport.members = $ipset.value
        $ipsetToExport.isUniversal = $ipset.scope.id
        $toprint += $ipsetToExport
        $index++
    }

    if($fileName){
        $fileName = $fileName.Replace("DFWR", "IPSets")
    } else {
        $fileName = "$($defaultnsxconnection.Server)-IPSets-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation
}

#this function imports ipset from csv
function importNSXIPSets{
    param($importpath)
    $ipsetsToImport = Import-Csv $importpath
    $controlset = (Get-NsxIpSet | ?{$_.isuniversal -eq "false"}).name
    $index = 1
    foreach($iptoim in $ipsetsToImport){
        Write-Host "[$($index)/$($ipsetsToImport.length)] importing $($iptoim.name)" -fore Cyan
        if($controlset -contains $iptoim.name){
            $src = Get-NsxIpSet -Name $iptoim.name
            $base = $src.value.split(',')
            $attack = $iptoim.members.split(',')
            foreach($mem in $attack){
                $src = Get-NsxIpSet -Name $iptoim.name
                $base = $src.value.split(',')
                if($base -contains $mem){
                    Write-Host "the ip $($mem) already exists, not adding" -fore Yellow
                } else {
                    $src = Get-NsxIpSet -Name $iptoim.name
                    $added = Add-NsxIpSetMember -IPSet $src -IPAddress $mem
                    if($added){
                        Write-Host "adding ip $($mem) to ipset $($iptoim.name)" -fore Green
                    } else {
                        "[$(Get-Date)] manual input of $($mem) to ipset $($iptoim.name) needed, addition to ipset failed" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($mem) to ipset $($iptoim.name) needed, addition to ipset failed" -fore Red
                    }
                }
            }
        } else {
            $added = New-NsxIpSet -Name $iptoim.name -IPAddress $iptoim.members
            if($added){
                Write-Host "adding ip $($iptoim.members) to ipset $($iptoim.name)" -fore Green
            } else {
                "[$(Get-Date)] manual input of $($iptoim.members) to ipset $($iptoim.name) needed, creation of ipset failed" | Out-File manualOverride.txt -Append
                Write-Host "manual input of $($iptoim.members) to ipset $($iptoim.name) needed, creation of ipset failed" -fore Red
            }
        }
        $index++
    }
}

#==============================================#
#Security Groups                               #
#==============================================#

function exportNSXSecurityGroups{
    [cmdletbinding()]
    param($SecurityGroupNames, $filename)
    $listOfSecurityGroups = @()
    if($SecurityGroupNames){
        foreach($sgss in $SecurityGroupNames){
            $listOfSecurityGroups += Get-NsxSecurityGroup -name $sgss
        }
    } else {
        $listOfSecurityGroups = Get-NsxSecurityGroup
    }

    $index = 1
    $toprint = @()
    foreach($securityGroup in $listOfSecurityGroups){
        Write-Host "[$($index)/$($listOfSecurityGroups.length)] Exporting $($securityGroup.name)" -fore Cyan
        $sg = New-Object SecurityGroupHelper
        $sg.name = $securityGroup.name
        #$sg.staticInclude = $securityGroup.member
        $staticIncludes = ""
        $count = 0
        foreach($static in $securityGroup.member){
            if($count -eq 0){
                $staticIncludes += "$($static.name)&$($static.objectTypeName)"
                $count++
            } else {
                $staticIncludes += ",$($static.name)&$($static.objectTypeName)"
            }
        }
        $sg.staticInclude = $staticIncludes
        #$staticIncludes
        #$sg.staticExclude = $securityGroup.excludeMember
        $staticExcludes = ""
        $count = 0
        foreach($static in $securityGroup.excludeMember){
            if($count -eq 0){
                $staticExcludes += "$($static.name)&$($static.objectTypeName)"
                $count++
            } else {
                $staticExcludes += ",$($static.name)&$($static.objectTypeName)"
            }
        }
        $sg.staticExclude = $staticExcludes
        #$staticExcludes
        #dybamicmembership
        $dynstring = ""
        $count = 0
        foreach($dynamicCriteria in $securityGroup.dynamicmemberdefinition.dynamicset){
            $dynCriteriaTemp = New-Object DynamicSecurityGroupHelper
            $strtoadd = ""
            #Write-Host "abou to do dynamic criteria $($dynamicCriteria.dynamiccriteria.length)" -fore Red

            if($dynamicCriteria.dynamiccriteria.length -gt 1){
                #this means multiple criteria within criteria
                #$dynamicCriteria
                $multicriteriastring = "$($dynamicCriteria.operator),"
                $count = 0
                foreach($dc in $dynamicCriteria.dynamiccriteria){
                #$dc
                    if($count -eq 0){
                        $multicriteriastring += "$($dc.operator),$($dc.key),$($dc.criteria),$($dc.value)"
                        $count++
                    } else {
                        $multicriteriastring += "&$($dc.operator),$($dc.key),$($dc.criteria),$($dc.value)"
                    }
                }
                $strtoadd = $multicriteriastring
            } else {
                $dynCriteriaTemp.setOperator = $dynamicCriteria.operator
                $dynCriteriaTemp.criteriaOperator = $dynamicCriteria.dynamicCriteria.operator
                $dynCriteriaTemp.key = $dynamicCriteria.dynamicCriteria.key
                $dynCriteriaTemp.criteria = $dynamicCriteria.dynamicCriteria.criteria
                $dynCriteriaTemp.value = $dynamicCriteria.dynamicCriteria.value
                $dynCriteriastring = "$($dynamicCriteria.operator),$($dynCriteriaTemp.criteriaOperator),$($dynCriteriaTemp.key),$($dynCriteriaTemp.criteria),$(convertNamingConvention($dynCriteriaTemp.value))"
                $strtoadd = $dynCriteriastring
            }
            if($count -eq 0){
                $dynstring += $strtoadd    
                $count++
            } else {
                $strtoadd = "%$($strtoadd)"
                $dynstring += $strtoadd
            }
        }
        $sg.dynamicMembership = $dynstring
        #$dynstring | Out-File "omfg.txt"
        $toprint += $sg
        $index++
    }

    if($fileName){
        $fileName = $fileName.Replace("DFWR", "SecurityGroups")
    } else {
        $fileName = "$($defaultnsxconnection.Server)-SecurityGroups-$((Get-Date).ToString("yyyy'-'MM'-'ddThh'-'mm'-'ss")).csv"
    }
    $toprint | Export-Csv $fileName -NoTypeInformation
}

function importNSXSecurityGroups{
    param($importpath)
    $securityGroupsToImport = Import-Csv $importpath
    $index = 1
    foreach($importingSecurityGroup in $securityGroupsToImport){
        Write-Host "[$($index)/$($securityGroupsToImport.length)] importing $($importingSecurityGroup.name)" -fore Cyan
        #creating the security group
        if(!(Get-NsxSecurityGroup -name $importingSecurityGroup.name)){
            Write-Host "creating security group $($importingSecurityGroup.name)"  -fore Green
            $secgroup = New-NsxSecurityGroup -Name $importingSecurityGroup.name 
        } else {
            Write-Host "$($importingSecurityGroup.name) security group already exists" -fore Yellow
        }

        #adding the static include members
        $staticIncludes = $importingSecurityGroup.staticinclude.split(',')
        if($importingSecurityGroup.staticinclude.length -gt 0){
            foreach($si in $staticIncludes){
                $info = $si.split('&')
                if($info[1] -eq "VirtualMachine"){
                    if((Get-VM).name -contains $info[0]){
                        $toadd = Get-VM -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.member.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, virtual machine $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, virtual machine $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "IPSet"){
                    if((Get-NsxIpSet).name -contains $info[0]){
                        $toadd = Get-NsxIpSet -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.member.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, ipset $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, ipset $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityGroup"){
                    if((Get-NsxSecurityGroup).name -contains $info[0]){
                        $toadd = Get-NsxSecurityGroup -name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.member.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                         "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, security group $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, security group $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityTag"){
                    if((Get-NsxSecurityTag).name -contains $info[0]){
                        $toadd = Get-NsxSecurityTag -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.member.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                         "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, security tag $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, security tag $($info[0]) does not exist" -fore Red   
                    }
                } elseif($info[1] -eq "DistributedVirtualPortgroup"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of distributed virtual portgroup failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of distributed virtual portgroup failed" -fore Red
                } elseif($info[1] -eq "ClusterComputeResource"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of cluster failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of cluster failed" -fore Red
                } elseif($info[1] -eq "Datacenter"){
                    "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of datacenter failed" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static include needed, addition of datacenter failed" -fore Red
                } else {
                    "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) needed, $($info[1]) $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) needed, $($info[1]) $($info[0]) does not exist" -fore Red 
                }
            }
        }

        #adding the static exclude members
        $staticExcludes = $importingSecurityGroup.staticexclude.split(',')
        if($importingSecurityGroup.staticexclude.length -gt 0){
            foreach($si in $staticExcludes){
                $info = $si.split('&')
                if($info[1] -eq "VirtualMachine"){
                    if((Get-VM).name -contains $info[0]){
                        $toadd = Get-VM -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.excludeMember.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, virtual machine $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) needed, virtual machine $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "IPSet"){
                    if((Get-NsxIpSet).name -contains $info[0]){
                        $toadd = Get-NsxIpSet -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.excludeMember.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, ipset $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, ipset $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityGroup"){
                    if((Get-NsxSecurityGroup).name -contains $info[0]){
                        $toadd = Get-NsxSecurityGroup -name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.excludeMember.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, security group $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, security group $($info[0]) does not exist" -fore Red
                    }
                } elseif($info[1] -eq "SecurityTag"){
                    if((Get-NsxSecurityTag).name -contains $info[0]){
                        $toadd = Get-NsxSecurityTag -Name $info[0]
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        if(!($sg.excludeMember.name -eq $($info[0]))){
                            Write-Host "adding $($info[0]) to $($importingSecurityGroup.name)" -fore Green
                            $sg | Add-NsxSecurityGroupMember -Member $toadd -MemberIsExcluded
                        } else {
                            Write-Host "$($info[0]) already exists as a part of $($importingSecurityGroup.name)" -fore Yellow
                        }
                    } else {
                        "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, security tag $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                        Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) static exclude needed, security tag $($info[0]) does not exist" -fore Red 
                    }
                } else {
                    "[$(Get-Date)] manual input of $($info[0]) to $($importingSecurityGroup.name) needed, $($info[1]) $($info[0]) does not exist" | Out-File manualOverride.txt -Append
                    Write-Host "manual input of $($info[0]) to $($importingSecurityGroup.name) needed, $($info[1]) $($info[0]) does not exist" -fore Red 
                }
            }
        }
    
        #adding the dynamic membership
        $dynMembership = $importingSecurityGroup.dynamicmembership.split('%')
        $dynamicmembersetindex = 0
        if($importingSecurityGroup.dynamicmembership.length -gt 0){
            foreach($dynmem in $dynMembership){
                $dynmem = $dynmem -replace "vm.name","vmname"
                $dynmem = $dynmem -replace "vm.guest_os_full_name","osname"
                $dynmem = $dynmem -replace "vm.guest_host_name","computername"
                $dynmem = $dynmem -replace "vm.security_tag","securitytag"
                $dynmem = $dynmem -replace "contains","contains"
                $dynmem = $dynmem -replace "ends_with","ends_with"
                $dynmem = $dynmem -replace "starts_with","starts_with"
                $dynmem = $dynmem -replace "!=","notequals"
                $dynmem = $dynmem -replace "=","equals"
                $dynmem = $dynmem -replace "similar_to","regex"
                                                                                                                                                                                                                            if($dynmem -match "&"){
                #Write-Host "This dynamic membership criteria has MULTIPLE criteria" -fore White
                $multc = $dynmem.split('&')
                foreach($criterium in $multc){
                    $info = $criterium.split(',')
                    if($info[1] -eq "ENTITY"){
                        Write-Host "manual intervention needed, failed to add where $($info[0]) $($info[1]) $($info[2]) $($info[3]) to security group $($importingSecurityGroup.name)" -fore Red
                        "[$(Get-Date)] manual intervention needed, failed to add where $($info[0]) $($info[1]) $($info[2]) $($info[3]) to security group $($importingSecurityGroup.name)" | Out-File manualOverride.txt -Append
                    } elseif($info.length -eq 5){
                        $setOperator = $info[0]
                        $setOperator = $setOperator -replace "any","or"
                        $setOperator = $setOperator -replace "all","and"
                        $criteriaOperator = $info[1]
                        $criteriaOperator = $criteriaOperator -replace "or","any"
                        $criteriaOperator = $criteriaOperator -replace "and","all"
                        $key = $info[2]
                        $criteria = $info[3]
                        $value = $info[4]
                        $spec = $null
                        $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        #Write-Host "adding dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        $updateddynset = $sg | Add-NsxDynamicMemberSet -SetOperator $setOperator -CriteriaOperator $criteriaOperator -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                        $dynamicmembersetindex++
                        if($spec){
                            Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        } else {
                            Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($importingSecurityGroup.name)" -fore Red
                            "[$(Get-Date)] manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($importingSecurityGroup.name)" | Out-File manualOverride.txt -Append
                        }
                    } else {
                        $criteriaOperator = $info[0]
                        $criteriaOperator = $criteriaOperator -replace "or","any"
                        $criteriaOperator = $criteriaOperator -replace "and","all"
                        $key = $info[1]
                        $criteria = $info[2]
                        $value = $info[3]
                        $spec = $null
                        $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                        $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                        $dynmembersetgg = $sg | Get-NsxDynamicMemberSet -Index $dynamicmembersetindex
                        #Write-Host "adding dynamic criteria where $($criteriaoperator) $($key) $($criteria) $($value) to index $($dynamicmembersetindex)" -fore green
                        $updateddynset = $dynmembersetgg | Add-NsxDynamicCriteria -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                        if($updateddynset){
                            Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                        } else {
                            Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($importingSecurityGroup.name)" -fore Red
                            "[$(Get-Date)] manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value) to security group $($importingSecurityGroup.name)" | Out-File manualOverride.txt -Append
                        }
                    }
                }
                        } elseif($dynmem -match "entity"){ 
                Write-Host "manual intervention needed, failed to add where criteria contains entity" -fore Red
                "[$(Get-Date)] manual addition where failed to add where criteria contains entity check security group $($importingSecurityGroup.name)" | Out-File manualOverride.txt -Append   
                                                                                                                } else {
                #Write-Host "This dynamic membership criteria has ONE criteria" -fore White
                $info = $dynmem.split(',')
                $setOperator = $info[0]
                $setOperator = $setOperator -replace "any","or"
                $setOperator = $setOperator -replace "all","and"
                $criteriaOperator = $info[1]
                $criteriaOperator = $criteriaOperator -replace "or","any"
                $criteriaOperator = $criteriaOperator -replace "and","all"
                $key = $info[2]
                $criteria = $info[3]
                $value = $info[4]
                $spec = $null
                $spec = New-NsxDynamicCriteriaSpec -Key $key -Condition $criteria -Value $value -WarningAction SilentlyContinue
                $sg = Get-NsxSecurityGroup -name $importingSecurityGroup.name
                #Write-Host "adding dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                $updateddynset = $sg | Add-NsxDynamicMemberSet -SetOperator $setOperator -CriteriaOperator $criteriaOperator -DynamicCriteriaSpec $spec -WarningAction SilentlyContinue
                $updateddynset
                $dynamicmembersetindex++
                if($spec){
                    Write-Host "sucessfully added the dynamic member set where $($criteriaoperator) $($key) $($criteria) $($value)" -fore green
                } else {
                    Write-Host "manual intervention needed, failed to add where $($criteriaoperator) $($key) $($criteria) $($value)" -fore Red
                    "[$(Get-Date)] manual addition where $($criteriaoperator) $($key) $($criteria) $($value) failed check security group $($importingSecurityGroup.name)" | Out-File manualOverride.txt -Append
                }
            }
            }
        }
        $index++
    }
}

clear
